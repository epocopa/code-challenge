const fs = require('fs');

//Read file from arguments
let f = process.argv[2]
if (!f) {
    let programName = process.argv[1].split('/').pop()//this can be done with _path_
    console.log('Usage: node ' + programName + ' input_filename');
    return;
}

//Read file contents
const data = fs.readFileSync(f, 'UTF-8');
const lines = data.split(/\n/);

//Save coordinates
const maxCoordinatesLine = splitLine(lines[0])
let maxCoordinates = {
    x: maxCoordinatesLine[0],
    y: maxCoordinatesLine[1]
}

//Initialize matrix
const matrix = new Array(maxCoordinates.x + 1);
for (let i = 0; i < matrix.length; i++) {
    matrix[i] = new Array(maxCoordinates.y + 1);
}

//Initialize map for fallen robots
let fallenSet = new Set()

let n = 1;
while (resolveCase(lines.slice(n, n+2), matrix, maxCoordinates, fallenSet)) {
    n += 2;
}


function resolveCase(lines, matrix, maxCoordinates,fallenSet) {
    if (lines.length == 0) {
        return false;
    }
    
    //Save robot position
    const coordinatesLine = splitLine(lines[0]);
    let coordinates = {
        x: coordinatesLine[0],
        y: coordinatesLine[1],
        orientation: coordinatesLine[2]
    }

    //Save path
    const path = lines[1];

    //Iterate path
    for (let i = 0; i < path.length; i++) {
        switch (path[i]) {
            case 'L':
                coordinates.orientation = moveLeft(coordinates.orientation)
                break;
            case 'R':
                coordinates.orientation = moveRight(coordinates.orientation)
                break;
            case 'F':
                if(!moveForward(coordinates, maxCoordinates, fallenSet)) {
                    console.log(coordinates.x + ' ' + coordinates.y + ' ' + coordinates.orientation + ' LOST');
                    return true;
                }
                break;
        }
    }

    console.log(coordinates.x + ' ' + coordinates.y + ' ' + coordinates.orientation);
    return true;
}

function splitLine(line) {
    return line.split(' ');
}

function moveLeft(orientation) {
    switch (orientation) {
        case 'N':
            return 'W'
            break;
        case 'E':
            return 'N'
            break;
        case 'S':
            return 'E'
            break;
        case 'W':
            return 'S'
            break;
    }
}

function moveRight(orientation) {
    switch (orientation) {
        case 'N':
            return 'E'
            break;
        case 'E':
            return 'S'
            break;
        case 'S':
            return 'W'
            break;
        case 'W':
            return 'N'
            break;
    }
}

function moveForward(coordinates, maxCoordinates, fallenSet) {
    //This is the unique key used in the set
    hash = coordinates.x+coordinates.y+coordinates.orientation
    if (fallenSet.has(hash)) {
        return true;
    }
    switch (coordinates.orientation) {
        case 'N':
            if (coordinates.y < maxCoordinates.y) {
                coordinates.y++;
                return true;
            } else {
                fallenSet.add(hash)
                return false;
            }
            break;
        case 'E':
            if (coordinates.x < maxCoordinates.x) {
                coordinates.x++;
                return true;
            } else {
                fallenSet.add(hash)
                return false;
            }
            break;
        case 'S':
            if (coordinates.y > 0) {
                coordinates.y--;
                return true;
            } else {
                fallenSet.add(hash)
                return false;
            }
            break;
        case 'W':
            if (coordinates.x > 0) {
                coordinates.x--;
                return true;
            } else {
                fallenSet.add(hash)
                return false;
            }
            break;
    }
}
